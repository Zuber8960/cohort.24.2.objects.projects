// Problem4


// function pairs(obj) {
//     // Convert an object into a list of [key, value] pairs.
//     // http://underscorejs.org/#pairs
// }


//===============================================================================



const obj = {
    a : 1, b : 2, c : 3
}

function pairs(obj){
    let arr = [];
    if(Array.isArray(obj) || typeof obj === "string"){
        for(let index = 0; index<obj.length; index++){
            arr.push([index, obj[index]]);
        }
    }else{
        for(let key in obj){
            arr.push([key,obj[key]]);
        }
    }

    return arr;
}

// console.log(pairs(obj));

module.exports = pairs;















