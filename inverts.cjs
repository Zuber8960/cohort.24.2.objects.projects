// problem 5


// function invert(obj) {
//     // Returns a copy of the object where the keys have become the values and the values the keys.
//     // Assume that all of the object's values will be unique and string serializable.
//     // http://underscorejs.org/#invert
// }


//========================================================================================


const obj = {
    a : 1, b : 2, c : 3
}

function invert(obj){
    const newObj = {};
    for(let key in obj){
        const newKey = obj[key];
        const newVal = key;
        newObj[newKey] = newVal;
    }
    // console.log(obj);
    return newObj;
}

// console.log(invert(obj));



module.exports = invert;



















