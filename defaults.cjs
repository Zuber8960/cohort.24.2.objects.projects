// problem 6


// function defaults(obj, defaultProps) {
//     // Fill in undefined properties that match properties on the `defaultProps` parameter object.
//     // Return `obj`.
//     // http://underscorejs.org/#defaults
// }

//========================================================================================


// const obj = {
//     a : 1, b : 2, c : 3
// }

function defaults(obj , passedObject){

    for(let key in passedObject){
        if(obj[key] === undefined){
            obj[key] = passedObject[key];
        }
    }
    // console.log(obj);
    return obj;
}

// console.log(defaults(obj, {c : 4}));



module.exports = defaults;



















