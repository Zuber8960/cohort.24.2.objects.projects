// problem2

// function values(obj) {
//     // Return all of the values of the object's own properties.
//     // Ignore functions
//     // http://underscorejs.org/#valuesz
// }



//===============================================================================================



// const obj = {
//     a : 1, b:2, c:3, d:4
// }

// const obj = {
//     1 : "a",2 : "b"
// }


// const obj = {
//     a: 1, b: 2, c: 3, d: 4, zuber() {
//         console.log('zuber ahmad');
//     }
// };




function values(obj) {
    const arr = [];
    if(Array.isArray(obj) || typeof obj === "string"){
        for(let index = 0; index<obj.length; index++){
            arr[index] = obj[index];
        }
    }else{
        for (let key in obj) {
            if (typeof obj[key] !== "function" ) {
                arr.push(obj[key]);
            }
            // console.log(typeof obj[key]);
        };
    }

    
    return arr;
}

// console.log(values(obj));


module.exports = values;







