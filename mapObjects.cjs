// Problem3

// function mapObject(obj, cb) {
//     // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
//     // http://underscorejs.org/#mapObject
// }



// mapObject_.mapObject(object, iteratee, [context]) source
// Like map, but for objects. Transform the value of each property in turn.

// _.mapObject({start: 5, end: 12}, function(val, key) {
//   return val + 5;
// });
// => {start: 10, end: 17}




//===================================================================================================


const obj = {
    a : 1, b : 2, c : 3
}

function mapObject(obj, cb){
    for(let key in obj){
        const newVal = obj[key];
        const newKey = key
        obj[key] = cb(newVal,newKey);
    }
    return obj;
}


// const ans = mapObject(obj,(val, key)=>{
//     return () => {};
// })


// console.log(ans);


module.exports = mapObject;
