// problem1

// function keys(obj) {
//     // Retrieve all the names of the object's properties.
//     // Return the keys as strings in an array.
//     // Based on http://underscorejs.org/#keys
// }


//===============================================================================================



// const obj = {
//     a : 1, b:2, c:3, d:
// }

// const obj = {
//     1 : "a",2 : "b"
// }

function keys(obj){
    const arr = [];

    if(Array.isArray(obj) || typeof obj === "string"){
        for(let index = 0; index<obj.length; index++){
            arr[index] = index;
        }
    }else{
        for(let key in obj){
            arr.push(key);
        }
    }

    
    return arr;
}

// console.log(keys(obj));


module.exports = keys;







